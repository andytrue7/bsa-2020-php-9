<?php

namespace Tests\Feature;

use App\Entities\Stock;
use App\Entities\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class MarketApiTest extends TestCase
{
    use RefreshDatabase;

    protected $user;

    protected function setUp(): void
    {
        parent::setUp();

        $this->user = factory(User::class)->create();
    }

    public function test_an_authenticated_user_can_add_stocks()
    {
        $this->withoutExceptionHandling();

        $this->actingAs($this->user, 'api');

        $stock = [
          'price' => 1.99,
          'start_date' => (new \DateTime('now'))->format('Y-m-d H:i:s')
        ];

        $response =  $this->withHeaders(['Content-Type' => 'application/json'])->
             json('POST','/api/stocks', $stock);

        $response
            ->assertStatus(201)
            ->assertJson(['data' => $stock]);

        $this->assertDatabaseHas('stocks', array_merge(['id' => 1], $stock));
    }

    public function test_an_authenticated_user_can_delete_stocks()
    {
        $this->withoutExceptionHandling();

        $this->actingAs($this->user, 'api');

        $stock = factory(Stock::class)->create(['user_id' => $this->user->id]);

        $response = $this->withHeaders(['content-type' => 'application/json'])
            ->call('DELETE','/api/stocks/' . $stock->id);

        $response->assertStatus(204);

        $this->assertDatabaseMissing('stocks', $stock->toArray());
    }

    public function test_an_user_can_get_stocks()
    {
        $params = [
            'start_date' => (new \DateTime())->getTimestamp(),
            'end_date' => (new \DateTime())->modify('+1 hour')->getTimestamp(),
            'frequency' => 60
        ];

        $response = $this->withHeaders(['content-type' => 'application/json'])
            ->json('GET', '/api/chart-data', $params);

        $response->assertStatus(200);

    }

    public function test_unable_to_add_stocks_with_negative_price()
    {
        $this->actingAs($this->user, 'api');

        $stock = [
            'price' => -1.99,
        ];

        $response =  $this->withHeaders(['Content-Type' => 'application/json'])->
        json('POST','/api/stocks', $stock);

        $response->assertStatus(400);

    }

    public function test_unable_to_add_stocks_with_previous_date()
    {

        $this->actingAs($this->user, 'api');

        $stock = [
            'start_date' => (new \DateTime())->modify('-1 second')->format('Y-m-d H:i:s')
        ];

        $response =  $this->withHeaders(['Content-Type' => 'application/json'])->
        json('POST','/api/stocks', $stock);

        $response->assertStatus(400);

    }

    public function test_start_date_is_not_greater_than_end_date()
    {

        $params = [
            'start_date' => (new \DateTime())->modify('+1 hour')->getTimestamp(),
            'end_date' => (new \DateTime())->getTimestamp(),
            'frequency' => 60
        ];

        $response = $this->withHeaders(['content-type' => 'application/json'])
            ->json('GET', '/api/chart-data', $params);

        $response->assertStatus(400);
    }

    public function test_invalid_frequency_should_return_error_correctly()
    {
        $params = [
            'start_date' => (new \DateTime())->getTimestamp(),
            'end_date' => (new \DateTime())->modify('+1 hour')->getTimestamp(),
            'frequency' => -1
        ];

        $response = $this->withHeaders(['content-type' => 'application/json'])
            ->json('GET', '/api/chart-data', $params);

        $response
            ->assertSee('Frequency must be greater than 0')
            ->assertStatus(400);
    }
}
