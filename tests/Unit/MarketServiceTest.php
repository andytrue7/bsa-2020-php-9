<?php

namespace Tests\Unit;


use App\DTO\Collections\ChartDataCollection;
use App\DTO\Values\ChartData;
use App\Entities\Stock;
use App\Repositories\Contracts\StockRepository;
use App\Repositories\Criteria\PeriodCriteria;
use App\Services\Exceptions\InvalidFrequencyException;
use Tests\TestCase;
use App\Services\MarketDataService;

class MarketServiceTest extends TestCase
{
    public function test_getChartData_returns_data_correctly_by_hour()
    {

        $stockCollection = collect([
            new Stock(['price' => 20, 'start_date'=>'2020-01-01 12:04:01']),
            new Stock(['price' => 14, 'start_date'=>'2020-01-01 12:07:01']),
            new Stock(['price' => 28, 'start_date'=>'2020-01-01 13:07:01'])
        ]);

        $startDate = (new \DateTime('2020-01-01 12:02:01'));
        $endDate = (new \DateTime('2020-01-01 14:02:12'));
        $frequency = 60*60;

        $stockRepository = $this->createMock(StockRepository::class);

        $stockRepository->expects($this->once())
            ->method('findByCriteria')
            ->with(new PeriodCriteria($startDate, $endDate))
            ->willReturn($stockCollection);

        $marketDataService = new MarketDataService($stockRepository);

        $this->assertEquals(new ChartDataCollection([
            new ChartData(new \DateTime('2020-01-01 12:02:01'), 17),
            new ChartData(new \DateTime('2020-01-01 13:02:01'), 28)
        ]),$marketDataService->getChartData($startDate, $endDate, $frequency));

    }

    public function test_getChartData_returns_data_correctly_by_day()
    {
        $stockCollection = collect([
            new Stock(['price' => 20, 'start_date'=>'2020-01-01 12:04:01']),
            new Stock(['price' => 14, 'start_date'=>'2020-01-01 12:07:01']),
            new Stock(['price' => 28, 'start_date'=>'2020-01-02 13:07:01'])
        ]);

        $startDate = (new \DateTime('2020-01-01 12:02:01'));
        $endDate = (new \DateTime('2020-01-03 14:02:12'));
        $frequency = 24*60*60;

        $stockRepository = $this->createMock(StockRepository::class);

        $stockRepository->expects($this->once())
            ->method('findByCriteria')
            ->with(new PeriodCriteria($startDate, $endDate))
            ->willReturn($stockCollection);

        $marketDataService = new MarketDataService($stockRepository);

        $this->assertEquals(new ChartDataCollection([
            new ChartData(new \DateTime('2020-01-01 12:02:01'), 17),
            new ChartData(new \DateTime('2020-01-02 12:02:01'), 28)
        ]),$marketDataService->getChartData($startDate, $endDate, $frequency));

    }

    public function test_getChartData_returns_data_correctly_by_week()
    {
        $stockCollection = collect([
            new Stock(['price' => 20, 'start_date'=>'2020-01-01 12:04:01']),
            new Stock(['price' => 14, 'start_date'=>'2020-01-04 12:07:01']),
            new Stock(['price' => 28, 'start_date'=>'2020-01-09 13:07:01'])
        ]);

        $startDate = (new \DateTime('2020-01-01 12:02:01'));
        $endDate = (new \DateTime('2020-01-10 14:02:12'));
        $frequency = 7*24*60*60;

        $stockRepository = $this->createMock(StockRepository::class);

        $stockRepository->expects($this->once())
            ->method('findByCriteria')
            ->with(new PeriodCriteria($startDate, $endDate))
            ->willReturn($stockCollection);

        $marketDataService = new MarketDataService($stockRepository);

        $this->assertEquals(new ChartDataCollection([
            new ChartData(new \DateTime('2020-01-01 12:02:01'), 17),
            new ChartData(new \DateTime('2020-01-08 12:02:01'), 28)
        ]),$marketDataService->getChartData($startDate, $endDate, $frequency));

    }

    public function test_getChartData_works_correctly_if_data_is_empty()
    {
        $stockCollection = collect([]);

        $startDate = (new \DateTime('2020-01-01 12:02:01'));
        $endDate = (new \DateTime('2020-01-10 14:02:12'));
        $frequency = 7*24*60*60;

        $stockRepository = $this->createMock(StockRepository::class);

        $stockRepository->expects($this->once())
            ->method('findByCriteria')
            ->with(new PeriodCriteria($startDate, $endDate))
            ->willReturn($stockCollection);

        $marketDataService = new MarketDataService($stockRepository);

        $this->assertEquals(new ChartDataCollection(),$marketDataService->getChartData($startDate, $endDate, $frequency));

    }

    public function test_getChartData_throws_exception_for_negative_frequency()
    {
        $stockCollection = collect([]);

        $startDate = (new \DateTime('2020-01-01 12:02:01'));
        $endDate = (new \DateTime('2020-01-10 14:02:12'));
        $frequency = 0;

        $stockRepository = $this->createStub(StockRepository::class);

        $stockRepository
            ->method('findByCriteria')
            ->willReturn($stockCollection);

        $marketDataService = new MarketDataService($stockRepository);

        $this->expectException(InvalidFrequencyException::class);

        $marketDataService->getChartData($startDate, $endDate, $frequency);
    }
}
