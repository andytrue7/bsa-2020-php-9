<?php

declare(strict_types=1);

namespace App\Actions;

use App\Exceptions\Api\LogicException;
use App\Repositories\Contracts\StockRepository;
use App\Actions\Requests\CreateStockRequest;
use App\Actions\Responses\CreateStockResponse;
use Illuminate\Contracts\Auth\Factory as AuthFactory;
use Illuminate\Contracts\Auth\Guard;
use App\Entities\Stock;

class CreateStockAction
{
	private StockRepository $stockRepository;
	private Guard $auth;

	public function __construct(StockRepository $stockRepository, AuthFactory $authFactory)
	{
		$this->stockRepository = $stockRepository;
		$this->auth = $authFactory->guard();
	}

	public function execute(CreateStockRequest $request): CreateStockResponse
	{

	    $user = $this->auth->user();
		$stock = new Stock();
		$stock->user_id = $user->id;
		$stock->price = $request->price;
		$stock->start_date = $request->startDate->format('Y-m-d H:i:s');


        $this->validationData($stock);

		$result = $this->stockRepository->create($stock);

		return new CreateStockResponse(
			$result
		);
	}

	private function validationData($stock)
    {
        if ( $stock->price < 0) {
            throw new LogicException();
        }

        if ($stock->start_date < (new \DateTime())->format('Y-m-d H:i:s')) {
            throw new LogicException();
        }


    }
}
